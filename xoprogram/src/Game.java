import java.util.Scanner;

public class Game {
	private Player X;
	private Player O;
	private Board board;

	public Game() {
		X = new Player('X');
		O = new Player('O');
	}

	public void play() {
		while (true) {
			playone();
		}
	}

	public void playone() {
		board = new Board(X, O);
		printWelcome();
		while (true) {
			printTable();
			printTurn();
			input();
			if (board.isFinish()) {
				break;
			}
			board.switchPlayer();
		}
		printTable();
		showWinner();
		showStat();
	}

	private void printWelcome() {
		System.out.println("Welcome to game XO!");
	}
	
	private void showStat() {
		System.out.println(X.getName() + " W,D,L: " + X.getWin() + "," + X.getDraw() + "," + X.getLose());
		System.out.println(O.getName() + " W,D,L: " + O.getWin() + "," + O.getDraw() + "," + O.getLose());
		System.out.println();
	}

	private void showWinner() {
		Player player = board.getWinner();
		System.out.println(player.getName() + " Win!");
	}

	private void printTable() {
		char[][] table = board.getTable();
		System.out.println("  1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < table.length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}

	private void printTurn() {
		Player player = board.getCurrentPlayer();
		System.out.println(player.getName() + " Turn!");
	}

	private void input() {
		Scanner kb = new Scanner(System.in);

		while (true) {
			try {
				System.out.print("Please input (R,C): ");
				String input = kb.nextLine();
				String[] str = input.split(" ");
				if (str.length != 2) {
					System.out.println("Please input again! (ex: 1 1)");
					continue;
				}
				int row = Integer.parseInt(str[0]) - 1;
				int col = Integer.parseInt(str[1]) - 1;

				if (board.setTable(row, col) == false) {
					System.out.println("Is not empty!");
					continue;
				}
				break;
			} catch (Exception e) {
				System.out.println("Please input again! (ex: 1 1)");
				continue;

			}

		}

	}
}
